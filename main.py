import RPi.GPIO as GIPO
from rgb import turn_red, turn_green, turn_blue, turn_off
from buzzer import play_note, kill_note, notes
from rttl import decode_rttl_unit, split_song, decode_input, decode_options
from time import sleep
import player as play_lib

sheet_1 = "Indiana:d=,o=5,b=250:e,8p,8f,8g,8p,1c6,8p.,d,8p,8e,1f,p.,g,8p,8a,8b,8p,1f6,p,a,8p,8b,2c6,2d6,2e6,e,8p,8f,8g,8p,1c6,p,d6,8p,8e6,1f.6,g,8p,8g,e.6,8p,d6,8p,8g,e.6,8p,d6,8p,8g,f.6,8p,e6,8p,8d6,2c6"
sheet_2 = "Barbie girl:d=4, o=5, b=125:8g#, 8e, 8g#, 8c#6, a, p, 8f#, 8d#, 8f#, 8b, g#, 8f#, 8e, p, 8e, 8c#, f#, c#, p, 8f#, 8e, g#, f#"
def main(args):
    return 0

if __name__ == '__main__':
    player = play_lib.RttlPlayer()
    import sys
    turn_off()
  
    player.add_song(sheet_1)
    player.add_song(sheet_2)
    player.play_next()
    player.play_next()
    
    sys.exit(main(sys.argv))
