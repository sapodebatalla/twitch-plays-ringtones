import rttl
import buzzer
from time import sleep

MAX_NOTES=64
DEF_DURATION = 4
DEF_OCTAVE = 4
DEF_BEAT = 120

class Song():
	def __init__(self, name, options, song):
		self._name = name
		self._duration, self._octave, self._beat = rttl.decode_options(options)
		self._song = song
		
	def get_name(self):
		return self._name
		
	def get_duration(self):
		return self._duration
		
	def get_octave(self):
		return self._octave
		
	def get_beat(self):
		return self._beat
		
	def get_song(self):
		return self._song
	
	def __eq__(self, other):
		return self.get_duration() == other.get_duration()

class RttlPlayer():
	def __init__(self):
		self.playlist = []
		self.is_playing = False
		self._current_song=''

	def play_next(self):
		sleep(.15)
		if not self.is_playing and self.playlist:
			self._current_song = self.pop_song()
			self.play_song(self._current_song.get_song(),self._current_song.get_duration(), self._current_song.get_octave(), self._current_song.get_beat())

	def add_song(self, sheet):
		name, options, song = rttl.decode_input(sheet)
		print('{},{},{},'.format(name,options,song))
		if name and options and song:
			song = Song(name, options, song)
			self.playlist.append(song)

	def pop_song(self):
		sng = self.playlist[0]
		self.playlist = self.playlist[1:]
		return sng

	def play_song(self, song, s_duration=DEF_DURATION, s_octave=DEF_OCTAVE, s_beat=DEF_BEAT):
		s_beat = 12000/s_beat
		rttl_song = rttl.split_song(song)
		if rttl.validate_song(rttl_song):
			rttl_song = rttl_song[:MAX_NOTES]
			for unit in rttl_song:
				pitch,octave,duration,dot = unit
				octave = int(octave)
				duration = int(duration)
				dot = int(dot)
				octave = octave if octave >= 0 else s_octave
				duration = duration if duration > 0  else s_duration
				buzzer.play_note(pitch,octave,duration_to_seconds(duration,s_beat,dot),s_beat/60)
				self.is_playing = True
		self.is_playing = False



def duration_to_seconds(dur, bpm, dot=0):
	beat = bpm/60
	if dot:
		return (beat / dur)  + (beat / dur) * 0.5
	else:
		return (beat / dur) 
