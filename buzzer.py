import RPi.GPIO as GPIO
from time import sleep

BUZZPIN = 21

GPIO.setmode(GPIO.BCM)
GPIO.setup(BUZZPIN, GPIO.OUT)

p = GPIO.PWM(BUZZPIN, 1)

def play(freq, time, beat):
	p.start(25)
	p.ChangeFrequency(freq)
	sleep(time)
	p.stop()
	
def play_note(note, octave, duration,beat):
	play(notes[note] * 2 ** (octave + (0**octave)), duration, beat)
	
	
def kill_note():
	p.stop()


notes = {
	'C': 16.35,
	'C#': 17.32,
	'Db': 17.32,
	'D': 18.35,
	'D#': 19.45,
	'E': 20.60,
	'F': 21.83,
	'F#': 23.12,
	'Gb': 23.12,
	'G': 24.50,
	'G#': 25.96,
	'Ab': 25.96,
	'A': 27.50,
	'A#': 29.14,
	'Bb':29.14,
	'B': 30.87,
	'P': 0.01
}
