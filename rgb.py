import RPi.GPIO as GPIO

REDPIN = 4
BLUEPIN = 17
GREENPIN = 27

pins = [REDPIN,BLUEPIN,GREENPIN]

GPIO.setmode(GPIO.BCM)
GPIO.setup(REDPIN, GPIO.OUT)
GPIO.setup(BLUEPIN, GPIO.OUT)
GPIO.setup(GREENPIN, GPIO.OUT)


def turn_red():
	turn_off()
	GPIO.output(REDPIN,GPIO.HIGH)
	
def turn_green():
	turn_off()
	GPIO.output(GREENPIN,GPIO.HIGH)

def turn_blue():
	turn_off()
	GPIO.output(BLUEPIN,GPIO.HIGH)
	
def turn_off():
	for pin in pins:
		GPIO.output(pin,GPIO.LOW)
