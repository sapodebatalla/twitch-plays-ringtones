from buzzer import notes
import re


def decode_rttl_unit(rttl_unit):
	rttl_unit = rttl_unit.strip()
	rttl_unit = rttl_unit.upper()
	regex_check = re.search("^((16|32)|1|2|4|8)?([A-G]|P)([#B][.][1-8]|[.][1-8]|[1-8]|[#B.]|[#B][.]|[B#][1-8])?$",rttl_unit)
	if regex_check:
		duration = re.search("^(16|32|1|2|4|8)",rttl_unit)
		duration = duration.group() if duration else -1
		dot = re.search("[.]",rttl_unit)
		dot = 1 if dot else 0
		pitch = re.search("([A-G]|P)([#B])?",rttl_unit).group()
		octave = re.search("([]|[1-8])$",rttl_unit)
		octave = re.search("([]|[1-8])$",rttl_unit).group() if octave else -1
		return pitch, octave, duration, dot
	else:
		return -1,-1,-1, -1
	
	
def split_song(song):
	spl_song = song.split(",")
	song_rttl = []
	for unit in spl_song:
		song_rttl.append(decode_rttl_unit(unit))
	return song_rttl
	
def validate_song(song_rttl):
	if (-1,-1,-1,-1) in song_rttl:
		return False
	else:
		return True
		
def validate_rttl_input(input_str):
	regex = '^[a-zA-Z0-9 ]{1,25}:d=((16|32)|1|2|4|8), ?o=[1-8], ?b=(([0-9]){3}|([89][0-9])):((((16|32)|1|2|4|8)?([a-g]|p)([#B][.][1-8]|[.][1-8]|[1-8]|[#B.]|[#B][.]|[B#][1-8])?(, ?|$))+)$'
	regex_check = re.search(regex,input_str)
	if regex_check:
		return True
	else:
		return False
		
def decode_input(input_str):
	if validate_rttl_input(input_str):
		sheet = input_str.split(":")
		name= sheet[0]
		options= sheet[1]
		song= sheet[2]
		return name, options, song
	else:
		return (0, 0, 0)
		
def decode_options(opt):
	options = opt.split(",")
	dr = int(options[0].strip().split('=')[1])
	oc = int(options[1].strip().split('=')[1])
	bt = int(options[2].strip().split('=')[1])
	return dr, oc , bt
